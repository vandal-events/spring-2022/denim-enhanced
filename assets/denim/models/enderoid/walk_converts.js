// This tool makes it easier to copy the walk positions over to different models.

const walkLeft = require('./enderoid_walk_left.json');
const walkRight = require('./enderoid_walk_right.json');

const fs = require('fs');

const rotations = [
    '22deg',
    '45deg'
];

const jaws = [
    '',
    '_jaw',
    '_jaw2'
];

const armsAndLegsLeft = walkLeft.elements.filter(a => 
    a.name == 'arm_left' || 
    a.name == 'arm_right' || 
    a.name == 'leg_right' ||
    a.name == 'leg_left'
);

const armsAndLegsRight = walkRight.elements.filter(a => 
    a.name == 'arm_left' || 
    a.name == 'arm_right' || 
    a.name == 'leg_right' ||
    a.name == 'leg_left'
);

let enderoidLookUps = [];

for (let i = 0; i < rotations.length; i++) {
    for (let j = 0; j < jaws.length; j++) {
        enderoidLookUps.push(`enderoid_look_up_${rotations[i]}${jaws[j]}`);
    }
}

for (const enderoid of enderoidLookUps) {
    for (let i = 0; i < 2; i++) {
        const mode = i == 0 ? 'left' : 'right';

        let walk = JSON.parse(fs.readFileSync(`./${enderoid}.json`));

        for (let j = 0; j < walk.elements.length; j++) {
            const element = walk.elements[j];

            if (element.name.includes('arm') || element.name.includes('leg')) {
                walk.elements[j] = mode == 'left' ? armsAndLegsLeft.find(b => b.name == element.name) : armsAndLegsRight.find(b => b.name == element.name);
            }
        }

        fs.writeFileSync(`./${enderoid}_walk_${mode}.json`, JSON.stringify(walk, null, 4));

        console.log(`Wrote ${enderoid}_walk_${mode}`);
    }
}

console.log('Done');