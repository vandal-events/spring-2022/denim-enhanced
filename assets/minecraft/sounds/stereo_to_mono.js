// This tool is to convert all the stereo sounds (not packaged in Denim officially) to mono.
// The reasoning behind this is due to the volume being physically unadjustable in Minecraft if they
// are not in mono audio.
// This is caused by an OpenAL limitation, and is thus completely unfixable by Mojang.

// Provided by Mojang is a rule of thumb:
/*
While we are on topic of resource packs, please make sure your custom sounds 
have same number of channels as vanilla ones - otherwise you may have problems with volume and 
position (see OpenAL specification, section 5.3.4 for reason)

Rule of thumb: if you can move away from sound (i.e. it plays in world at certain position), 
keep it mono. If it's UI, background music or ambient sound it can be stereo.
*/

// The background music for Denim is going to be provided as Mono too, due to accessibility being
// a priority in Denim. If the music is unadjustable, then it is considered an accessibility issue,
// and will thus be required to be fixed.

const fs = require('fs');
const cp = require('child_process');

const { resolve, parse } = require('path');
const { readdir } = fs.promises;

// https://stackoverflow.com/a/45130990/17841246
// What, do you expect developer code to be 100% original? At least 2% of code is always copied from
// somewhere.
async function* getFiles(dir) {
    const dirents = await readdir(dir, { withFileTypes: true });
    for (const dirent of dirents) {
        const res = resolve(dir, dirent.name);
        if (dirent.isDirectory()) {
            yield* getFiles(res);
        } else {
            yield res;
        }
    }
}

(async () => {
    for await (const file of getFiles('./')) {
        if (!file.endsWith('_stereo.ogg')) continue;

        const monoFile = file.replace('_stereo.ogg', '.ogg');
        if (fs.existsSync(monoFile)) continue;

        cp.exec(`ffmpeg -i ${file} -ac 1  ${monoFile}`, (e, stdout, stderr) => {
            if (e) {
                console.error(e);
                console.error(stderr);

                return;
            }

            console.log(`Converted ${parse(file).base} to ${parse(monoFile).base}`);
        });
    }
})();